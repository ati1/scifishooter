﻿using UnityEngine;
using System.Collections;

public class AutoDestroyOnTimer : MonoBehaviour {

	public float timer = 0;

	void Update () 
	{
		if (timer < 0)
			timer = 0;
		if (timer == 0) 
		{
			if(GetComponent<NetworkView>())
			{
				DestroyAndRemoveRpcs();
			}
			else
			{
				Destroy(gameObject);
			}
		}
			
		timer -= Time.deltaTime;
	}
	void DestroyAndRemoveRpcs()
	{
		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		Network.Destroy(gameObject);
	}
}