﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoPickup : MonoBehaviour {
	
	public string pickAmmoName = "";
	private int pickAmmoNumber = 0;
	public int addammo = 0;
	
	public Rect popUpRect = new Rect(0,0,0,0);

	[System.Serializable]
	public class AmmoIncrease : Inventory.AmmoAmount
	{
		public int minAmmoIncrease = 0;
		public int maxAmmoIncrease = 0;
	}

	public List<AmmoIncrease> ammoIncreaseList = new List<AmmoIncrease>();

	public int minAmmoIncrease = 0;
	public int maxAmmoIncrease = 0;
	private bool m_showPopUp = false;
	private bool m_takeAmmo = false;
	

	void Start()
	{
		SetAmmoIncreases();
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			if(!col.gameObject.GetComponent<Inventory>().IsAmmoFull(pickAmmoName))
			{
				m_showPopUp = true;
			}
		}
		
	}
	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
			if(m_takeAmmo && !col.gameObject.GetComponent<Inventory>().IsAmmoFull(pickAmmoName))
			{
				m_takeAmmo = false;
				AddAmmoTo(col);
				if(addammo <= 0)
					Destroy(gameObject);
			}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			m_showPopUp = false;
			m_takeAmmo = false;
		}
	}
	void SetAmmoIncreases()
	{
		pickAmmoNumber = Random.Range(0,ammoIncreaseList.Count);
		pickAmmoName = ammoIncreaseList[pickAmmoNumber].ammoName;
		
		foreach(AmmoIncrease ammoIncrease in ammoIncreaseList)
		{
			if(ammoIncrease.ammoName == pickAmmoName)
			{
				maxAmmoIncrease = ammoIncrease.maxAmmoIncrease;
				minAmmoIncrease = ammoIncrease.minAmmoIncrease;
			}
		}
		addammo = Random.Range(minAmmoIncrease,maxAmmoIncrease);
	}
	void AddAmmoTo(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			GameObject currentWeapon = col.gameObject.GetComponentInChildren<Gun>().gameObject;
			List<Inventory.AmmoAmount> ammoList = col.gameObject.GetComponent<Inventory>().GetAmmoList();
			foreach(Inventory.AmmoAmount ammo in ammoList)
			{
				if(pickAmmoName == ammo.ammoName)
				{
					if(currentWeapon.GetComponent<Gun>().ammoName == ammo.ammoName)
					{
						int totalAmmo = currentWeapon.GetComponent<Gun>().ammo += addammo;
						if(totalAmmo <= ammo.maxAmmoAmount)
						{
							currentWeapon.GetComponent<Gun>().ammo += addammo;
							addammo = 0;
						}
						else
						{
							addammo = currentWeapon.GetComponent<Gun>().ammo + addammo - ammo.maxAmmoAmount;
							currentWeapon.GetComponent<Gun>().ammo = ammo.maxAmmoAmount;
						}

					}
					else
					{
						int totalAmmo = ammo.ammoAmount + addammo;
						if(totalAmmo <= ammo.maxAmmoAmount)
						{
							ammo.ammoAmount += addammo;
							addammo = 0;
						}
						else
						{
							addammo = ammo.ammoAmount + addammo - ammo.maxAmmoAmount;
							ammo.ammoAmount = ammo.maxAmmoAmount;
						}
					}
				}
			}
		}
	}
	void OnGUI()
	{
		if(m_showPopUp)
		{
			if(GUI.Button(popUpRect,"Pick up : " + addammo.ToString() +"x " + pickAmmoName))
			{
				m_showPopUp = false;
				m_takeAmmo = true;
			}
		}
	}
}