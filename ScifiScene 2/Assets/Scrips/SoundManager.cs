﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

	[System.Serializable]
	public class Sound
	{
		public string soundName = "";
		public AudioSource audioSource = null;

		public float volume = 1;
		public float pitch = 1;
		public float priority = 128;

		public bool isMuted = false;
		public bool PlayOnAwake = true;
		public bool looping = false;
		public bool removeOnComplete = false;

	}
	[System.Serializable]
	public class Music
	{
		
	}

	public List<Sound> sounds = null;
	List<GameObject> instantiatedSounds = new List<GameObject>();
	public List<Music> musics = null;
	
	void Start()
	{
		foreach(SoundManager.Sound sound in sounds)
		{
			if(sound.audioSource == null)
				Debug.LogError(sound.soundName + " GameObject doesn't have AudioSource attached to it. You propably should attach AudioSource to it.");
			else
			{
				sound.volume = sound.audioSource.volume;
				sound.pitch = sound.audioSource.pitch;
				sound.priority = sound.audioSource.priority;
				sound.isMuted = sound.audioSource.mute;
				sound.PlayOnAwake = sound.audioSource.playOnAwake;
				sound.looping = sound.audioSource.loop;
			}
		}
	}
	void Update()
	{
		for(int i = 0;i<instantiatedSounds.Count;i++)
		{
			if(!instantiatedSounds[i].GetComponent<AudioSource>().isPlaying)
			{
				Destroy(instantiatedSounds[i]);
				instantiatedSounds.RemoveAt(i);
			}
		}
	}

	[RPC]
	public void PlaySoundAtSpot(Vector3 position, string soundName)
	{
		GameObject instantiatedSound = null;
		foreach(SoundManager.Sound sound in sounds)
		{
			if(sound.soundName == soundName)
			{
				instantiatedSound = GameObject.Instantiate(sound.audioSource.gameObject,position,Quaternion.identity) as GameObject;
				if(sound.removeOnComplete)
					instantiatedSounds.Add(instantiatedSound);
				break;
			}
		}
	}
}