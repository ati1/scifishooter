﻿using UnityEngine;
using System.Collections;

public class AnimationScript : MonoBehaviour {

	public Animator anim = null;
	public Player player = null;
	//public float speed = 0;
	public Gun gun;

	public float speed = 0;
	private Rigidbody rgbody;
	//static int jump = Animator.StringToHash("Base.jumpAnim"); 

	// Use this for initialization
	void Awake() 
	{
		player = GetComponent<Player> ();
		gun = GetComponentInChildren<Gun> ();
		if(GetComponentInChildren<Animator>())
			anim = GetComponentInChildren<Animator>();
		else
			Debug.LogError("There is no Animator added to the child object");

		rgbody = transform.GetComponent<Rigidbody>();
	}

	void Update () 
	{
		if (GetComponent<NetworkView>().isMine) 
		{
			speed = rgbody.velocity.magnitude;
			if(gun == null)
				gun = GetComponentInChildren<Gun>();
			DoAnimations();
		}
	}
	public void DoAnimations()
	{
		anim.SetFloat("speed", speed);
		if (Input.GetKey (KeyCode.Mouse0) && gun.ammoCase != 0)
		{
			anim.SetBool("shooting",true);
		}
		else
		{
			anim.SetBool("shooting", false);
		}

//		if (Input.GetKeyDown (KeyCode.Space) && player.grounded && speed == 0) 
//		{
//			anim.SetBool("jumping", true);
//		}
//		else
//		{
//			anim.SetBool("jumping", false);
//		}
			
//		if(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)|| Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
//				anim.SetBool("isRunningGun" , true);
//			else if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
//			{
//				anim.SetBool("isWalkingGun", true);
//				anim.SetBool("isRunningGun", false);
//			}
//			else
//			{
//				anim.SetBool("isWalkingGun", false);
//				anim.SetBool("isRunningGun", false);
//			}
//			if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
//				anim.SetBool("walkSideGun", true);
//			else
//				anim.SetBool("walkSideGun", false);
//
//			if(GetComponentInChildren<Gun>())
//			{
//				if(Input.GetKey(KeyCode.Mouse0) && GetComponentInChildren<Gun>().ammoCase != 0)
//					anim.SetBool("shooting", true);
//				else
//					anim.SetBool("shooting", false);
//			}
//
//			if(Input.GetKeyDown(KeyCode.Space) && GetComponentInChildren<Player>().grounded)
//				anim.SetBool("jumping", true);
//			else
//				anim.SetBool("jumping", false);
	}
}
