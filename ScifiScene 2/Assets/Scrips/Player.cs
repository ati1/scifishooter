﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public Animator anim;
	public GameObject playerCamera;
	private GameObject currentCamera;
	public float currentSpeed;

	public float speed = 0;
	public float runSpeed = 0;
	public float rotateSpeed = 0;
	public float fallingSpeed = 0;
	public bool grounded;

	private Transform m_transform;
	private Rigidbody m_rigidbody;

	public ScoreBoard scoreboard;
	private bool showScoreboard;

	public int score = 0;
	public int deaths = 0;
	public Material[] materials;
	private Vector3 m_moveDirection = Vector3.zero;

	public LayerMask layer;

	private float m_lastSynchronizationTime = 0f;
	private float m_syncDelay = 0f;
	private float m_syncTime = 0f;
	private Vector3 m_syncStartPosition = Vector3.zero;
	private Vector3 m_syncEndPosition = Vector3.zero;
	private Quaternion m_rotation = Quaternion.identity;

	public string player;
	public int playerId;
	public Player[] allPlayers;

	public float secondsLeft = 0.5f;

	public bool enableMovements = true;

	public float yVelocity = 0;
	public float jumpSpeed = 50;
	
	public void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		bool shooting = anim.GetBool("shooting");
		bool jumping = anim.GetBool("jumping");
		float speed = anim.GetFloat("speed");
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		Quaternion syncRotation = m_rotation;

		if (stream.isWriting)
		{
			syncPosition = m_rigidbody.position;
			stream.Serialize(ref syncPosition);
			
			syncPosition = m_rigidbody.velocity;
			stream.Serialize(ref syncVelocity);

			syncRotation = GetComponent<Rigidbody>().rotation;
			stream.Serialize(ref syncRotation);

			stream.Serialize(ref speed);
			stream.Serialize(ref shooting);
			stream.Serialize(ref jumping);

		}
		else
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			stream.Serialize(ref syncRotation);
			m_rotation = syncRotation;

			m_syncTime = 0f;
			m_syncDelay = Time.time - m_lastSynchronizationTime;
			m_lastSynchronizationTime = Time.time;		
			m_syncEndPosition = syncPosition + syncVelocity * m_syncDelay;
			m_syncStartPosition = m_rigidbody.position;

			syncPosition = m_rigidbody.position;

			stream.Serialize(ref speed);
			anim.SetFloat("speed",speed);

			stream.Serialize(ref shooting);
			anim.SetBool("shooting",shooting);

			if(shooting)
			{
				//error
				GetComponentInChildren<ParticleSystem>().Play();
			}

			stream.Serialize(ref jumping);
			anim.SetBool("jumping",jumping);

		}
	}
	
	void Awake()
	{
		Cursor.visible = false;
		if(GetComponentInChildren<Animator>())
			anim = GetComponentInChildren<Animator>();
		else
			Debug.LogError("There is no Animator added to the child object");

		scoreboard = FindObjectOfType<ScoreBoard> ();

		m_transform = transform;
		m_rigidbody = GetComponent<Rigidbody>();
		//networkView.RPC("RequestDebug",RPCMode.All);
		m_lastSynchronizationTime = Time.time;
		grounded = false;

		currentCamera = GameObject.Instantiate(playerCamera) as GameObject;
		
		if(GetComponent<NetworkView>().isMine)
		{
			currentCamera.gameObject.SetActive(true);
			currentCamera.GetComponent<cameraOrbit>().lookAt = m_transform.FindChild("CameraLookAt");
		}
		else
		{
			currentCamera.gameObject.SetActive(false);
		}



//		if (networkView.isMine) 
//		{
//			Screen.showCursor = false;
//		}
	}
	public NetworkPlayer GetNetworkPlayer()
	{
		return GetComponent<NetworkView>().owner;
	}
	[RPC] void SetMaterial(int matId)
	{
		switch (matId)
		{
		case 0:
			m_transform.GetComponentInChildren<SkinnedMeshRenderer> ().material = materials[0];
			break;
		case 1:
			m_transform.GetComponentInChildren<SkinnedMeshRenderer> ().material = materials[1];
			break;
		case 2:
			m_transform.GetComponentInChildren<SkinnedMeshRenderer> ().material = materials[2];
			break;
		case 3:
			m_transform.GetComponentInChildren<SkinnedMeshRenderer> ().material = materials[3];
			break;
		}

	}

	[RPC] public void SetPlayerName(string name)
	{
		player = name;
		//FindObjectOfType<ScoreBoard>().networkView.RPC("RefresScoreboard",RPCMode.All);
		FindObjectOfType<ScoreBoard> ().RefresScoreboard ();
	}

	[RPC] public void DisconnectPlayer()
	{
		Network.Disconnect(1);
	}
	void OnDisconnectedFromServer(NetworkDisconnection info)
	{
		Destroy (currentCamera);
		NetworkManager manager = FindObjectOfType<NetworkManager> ();
		manager.cameraSetUp = false;
		manager.InitUI ();
		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		Network.Destroy (GetComponent<NetworkView>().viewID);
	}
	void Update()
	{
		if (GetComponent<NetworkView>().isMine) 
		{
			if (Input.GetKey (KeyCode.H)) 
				showScoreboard = true;
			else
				showScoreboard = false;
		}
	}
	void FixedUpdate()
	{
		if (GetComponent<NetworkView>().isMine)
		{
			if(enableMovements)
				DoMovement();
		}
		else
		{
			SyncedMovement();
			//transform.position = Vector3.Lerp(transform.position,m_SyncEndPosition,0.1f);
			//transform.rotation = Quaternion.Lerp(transform.rotation,rotation,0.1f);
		}
	}
	private void DoMovement()
	{
		m_rigidbody.velocity = Vector3.zero;
		RaycastHit ray;
		Vector3 dist = m_moveDirection;
		//if(Physics.Raycast(transform.position,Vector3.down,out ray,layer))
		if(Physics.SphereCast(m_transform.position, 0.7f,Vector3.down,out ray,0.5f,layer))
			grounded = true;
		else
			grounded = false;
		
		if(Input.GetKeyDown(KeyCode.Space) && grounded)
		{
			//StartCoroutine("jump");
			anim.SetBool("jumping",false);
			yVelocity = jumpSpeed;
		}
		if (grounded && yVelocity < 0) 
		{
			yVelocity = -0.1f;
		}
		else
		{
			if(Mathf.Abs(yVelocity) > jumpSpeed*0.75f)
			{
				anim.SetBool("jumping",true);
			}
			yVelocity += Physics.gravity.y * fallingSpeed * Time.deltaTime;
		}

		dist.y = yVelocity * Time.deltaTime;
		//m_rigidbody.velocity = new Vector3(m_rigidbody.velocity.x,(yVelocity * Time.deltaTime),m_rigidbody.velocity.z);

		if(Input.GetKey(KeyCode.D))
			m_transform.Translate(Vector3.right * Time.deltaTime);
			//transform.Rotate(Vector3.up, Mathf.Clamp(180f * Time.deltaTime, 0f, 360f));
		if(Input.GetKey(KeyCode.A))
			m_transform.Translate(Vector3.left * Time.deltaTime);
			//transform.Rotate(Vector3.up, -Mathf.Clamp(180f * Time.deltaTime, 0f, 360f));
		
		m_moveDirection = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		m_moveDirection = m_transform.TransformDirection(m_moveDirection);

		if (Input.GetKey (KeyCode.LeftShift)) 
		{
			m_moveDirection *= runSpeed;
		}
		else
		{
			m_moveDirection *= speed;
		}
			
//		if(!grounded)
//			m_transform.Translate(Vector3.down * fallingSpeed * Time.deltaTime);

		m_rigidbody.velocity = dist;
		currentSpeed = m_rigidbody.velocity.magnitude;
	}

	private void SyncedMovement()
	{
		m_syncTime += Time.deltaTime;
		
		GetComponent<Rigidbody>().position = Vector3.Lerp(m_syncStartPosition, m_syncEndPosition, m_syncTime / m_syncDelay);
		GetComponent<Rigidbody>().rotation = Quaternion.Lerp(m_transform.rotation, m_rotation, m_syncTime / m_syncDelay);
	}

	void OnGUI()
	{
		if (showScoreboard) 
		{
			GUILayout.BeginArea(new Rect(400,200,700,1500));
			foreach (ScoreBoard.score score in scoreboard.scores) 
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label("Name: " + score.name + " Kills: " + score.kills + " Deaths: " + score.deaths,GUILayout.Width(200));
				GUILayout.EndHorizontal();
			}
			GUILayout.EndArea();
		}
		if(Network.isClient)
		{
			if(Input.GetKey(KeyCode.Escape))
			{
				Cursor.visible = true;
				if(GUI.Button (new Rect(500,500,100,35),"Disconnect"))
				{
					GetComponent<NetworkView>().RPC("DisconnectPlayer",RPCMode.All);
					DisconnectPlayer();
				}
			}
			else
				Cursor.visible = false;
		}
	}
}