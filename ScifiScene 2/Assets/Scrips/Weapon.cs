﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public enum WeaponType
	{
		Sword,
		Gun
	};
	
	public WeaponType weaponType;
	private int weaponTypeInt;
	public Texture weaponLogo = null;
	
	public float dmg = 0;

	public override string ToString ()
	{
		return weaponType.ToString() + "," + weaponLogo.name + "," + dmg;
	}

	public void Parse(string parse)
	{
		string[] values = parse.Split(',');

		weaponType = (Weapon.WeaponType)System.Enum.Parse(typeof(WeaponType),values[0]);
		dmg = float.Parse(values[2]);
	}

	//public Weapon[] weaponList = {new Sword(),new Gun()};

//	void Update () 
//	{
//		weaponTypeInt = (int)weaponType;
//	
//		Debug.Log("Weapon " + weaponList[weaponTypeInt].ToString());
//	
//		if(Input.GetKeyDown(KeyCode.R))
//			weaponType = WeaponType.Sword;
//		else if(Input.GetKeyDown(KeyCode.E))
//			weaponType = WeaponType.Gun;
//	}
}