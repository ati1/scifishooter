﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gun : Weapon {

	public bool isEnabled = true;
	public Inventory inventory = null;

	public int id = 0;

	public List<GameObject> ammoFXList = null;

	public Player shooter = null;
	//private Player m_lastHitted = null;

	public int weaponId;
	public GameObject raycastShootFx = null;
	public float range = 0;

	public GUISkin customSkin = null;
	public GameObject ammoPrefab;
	public bool rdyToShoot = false;
	public float shootDelay = 0.2f;
	public float reloadtime = 0f;
	public SoundManager soundManager = null;

	public int ammoCase = 0;
	public int maxAmmoCase = 0;
	public Rect reloadTextPos = new Rect(0,0,0,0);
	public string reloadText = "";
	public GUIStyle customGUI = null;
	public Rect maxAmmoTextPos = new Rect(0,0,0,0);
	public Rect ammoTextPos = new Rect(0,0,0,0);

	private bool m_reloading = false;
	private bool m_reload = false;
	//private GameObject m_tempAmmo = null;
	//private Quaternion m_weaponQuat;
	private float m_shootTimer = 0;

	public string ammoName = "";
	public int ammo = 0;
	public string ammoSoundName = "";

	public ParticleSystem muzzleFlare;
	public Transform muzzlePos;

	public List<Inventory.AmmoAmount> ammoList = new List<Inventory.AmmoAmount>();

	void Start()
	{
		inventory = transform.root.GetComponent<Inventory>();
		//error
		ammoList = inventory.ammoAmountList;
		soundManager = GameObject.FindObjectOfType(typeof(SoundManager)) as SoundManager;
		ammo = inventory.SendAmmo(ammoName);
		ammoPrefab = inventory.SendAmmoGO(ammoName);
		shooter = transform.root.GetComponent<Player> ();
		if (shooter == null)
			Debug.LogError ("Gun is not attached to player");
		muzzlePos = muzzleFlare.transform;

	}
	
	void Update()
	{
		if(isEnabled)
			Shoot ();
	}

//	[RPC]public void RpcShoot (string ammoName, Vector3 position, Vector3 direction,Vector3 rot)
//	{
//		List<Inventory.AmmoAmount> ammoList = transform.root.GetComponent<Inventory>().ammoAmountList;
//
//		GameObject tempAmmo = null;
//
//		foreach(Inventory.AmmoAmount ammo in ammoList)
//		{
//			if(ammo.ammoName == ammoName)
//			{
//				tempAmmo = ammo.ammoGO;
//				break;
//			}
//		}
//
//		tempAmmo = GameObject.Instantiate (tempAmmo, position + direction * 5, Quaternion.Euler (rot))as GameObject;
//		tempAmmo.GetComponent<Ammo> ().shooter = transform.root.GetComponent<Player> ();
//		Debug.Log ("Shooter is: " + transform.root.GetComponent<Player> ().player);
//		tempAmmo.GetComponent<Rigidbody> ().AddForce (direction * tempAmmo.GetComponent<Ammo> ().ammoSpeed);
//		//m_tempAmmo.GetComponent<Ammo>().weapon = transform;
//		Debug.Log ("Shooting");
//	}
	//[RPC]
	public void Shoot()
	{
		//m_weaponQuat = transform.rotation;
		if(rdyToShoot && Input.GetKey(KeyCode.Mouse0) && ammoCase > 0)
		{
			rdyToShoot = false;
			m_shootTimer = shootDelay;
				
			//DoRaycastShoot();
			RaycastShoot();
				
			//	soundManager.networkView.RPC("PlaySoundAtSpot",RPCMode.All, transform.position,ammoSoundName);
			//	networkView.RPC("RpcShoot",RPCMode.All,ammoPrefab.name,gameObject.transform.position,transform.forward,m_weaponQuat.eulerAngles);
				
			//m_tempAmmo = Network.Instantiate(ammoPrefab, gameObject.transform.position + transform.forward * 5, m_weaponQuat,0)as GameObject;
			//m_tempAmmo.GetComponent<Ammo>().weapon = transform;
			if(ammoCase < 0)
				ammoCase = 0;
				
			ammoCase -= 1;
		}
		if(ammoCase < maxAmmoCase && ammo > 0)
		{
			m_reload = true;
			if(Input.GetKeyDown(KeyCode.R))
			{
				Reload();
			}
		}
		else
			m_reload = false;
			
		if(!rdyToShoot && m_shootTimer < 0f)
			rdyToShoot = true;
			
		m_shootTimer--;

	}
	public void DoRaycastShoot()
	{
		GetComponent<NetworkView>().RPC ("RaycastShoot", RPCMode.All);
	}
	//[RPC]
	public void RaycastShoot()
	{
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit hit;
		Physics.Raycast (ray, out hit,range);

		//GunFX (transform.position, hit.point);
		DoGunFX (ammoName,transform.position, hit.point);

		if (hit.collider != null) 
		{
			if (hit.collider.GetComponent<Player> ()) 
			{
				Stats temp = hit.collider.GetComponent<Stats>();
				temp.GetComponent<NetworkView>().RPC("AdjustHealth",RPCMode.All, -dmg);
				temp.GetComponent<NetworkView>().RPC("SetLastHitted",RPCMode.All,shooter.player);
			}
		}
	}
	public void DoGunFX(string ammoName,Vector3 startPos, Vector3 endPos)
	{
		muzzleFlare.Play();
		GetComponent<NetworkView>().RPC ("GunFX", RPCMode.All,ammoName, startPos, endPos);
	}
	[RPC] public void GunFX(string ammoName,Vector3 startPos, Vector3 endPos)
	{
//		Inventory tempInv = transform.root.GetComponent<Inventory>();
//		List<GameObject> ammoFXListTest = tempInv.ammoFXList;
//		GameObject ammoPrefab = null;
//
//		foreach(GameObject ammoFx in ammoFXListTest)
//		{
//			if(ammoFx.name == tempInv.currentWeapon.GetComponent<Gun>().raycastShootFx.name)
//			{
//				ammoPrefab = ammoFx;
//				break;
//			}
//		}
//		List<Inventory.AmmoAmount> ammoList = transform.root.GetComponent<Inventory>().ammoAmountList;

		GameObject ammoPrefab = null;

		if (ammoPrefab == null) 
		{
			foreach(Inventory.AmmoAmount ammo in ammoList)
			{
				if(ammo.ammoName == ammoName)
				{
					ammoPrefab = ammo.ammoGO;
				}
			}
//			for(int i = 0; i<ammoList.Count;i++)
//			{
//				if(ammoList[i].ammoID == weaponId)
//					ammoPrefab = ammoList[i].ammoGO;
//			}
		}
		//error
		ammoPrefab = GameObject.Instantiate (ammoPrefab, transform.position, transform.rotation) as GameObject;
		LineRenderer ln = ammoPrefab.GetComponent<LineRenderer>();
		ln.SetPosition (0, startPos);
		if (endPos != new Vector3 (0, 0, 0))
			ln.SetPosition (1, endPos);
		else
			ln.SetPosition (1, transform.position + transform.forward * range);
	}

//	Transform GetClosestHitPoint(Ray ray, out Vector3 hitPoint)
//	{
//		RaycastHit[] hits = Physics.RaycastAll (ray);
//		Transform closestHit = null;
//		float distance = 0;
//		hitPoint = Vector3.zero;
//
//		foreach (RaycastHit hit in hits) 
//		{
//			if(hit.transform != this.transform && (closestHit == null || hit.distance < distance))
//			{
//				closestHit = hit.transform;
//				distance = hit.distance;
//				hitPoint = hit.point;
//			}
//		}
//		return closestHit;
//	}
//	[RPC]public void SetLastHitted()
//	{
//		m_lastHitted.GetComponent<Stats> ().lastHitted = shooter;
//	}
	public void Reload()
	{
		if(!m_reloading)
		{
			m_reloading = true;
			StartCoroutine("ReloadTimer");
		}
	}
	void OnGUI()
	{
			if(transform.root.GetComponent<NetworkView>().isMine)
			{
				GUI.skin = customSkin;
				if(m_reload)
					GUI.Label(reloadTextPos, reloadText);
				GUI.Box(ammoTextPos,"Case: " + ammoCase.ToString() + " / " + maxAmmoCase);
				GUI.Box(maxAmmoTextPos,"Ammo: " + ammo.ToString());
			}
	}

	public int ReturnAmmo(string ammoName)
	{
		int returnValue = 0;
		if(ammoPrefab.name == ammoName)
		{
			returnValue = ammo + ammoCase;
		}
		else
		{
			returnValue = ammo;
		}
		return returnValue;
	}

	IEnumerator ReloadTimer()
	{
		int tempNeededAmmo = maxAmmoCase - ammoCase;
		int tempAmmo = ammo - tempNeededAmmo;
		yield return new WaitForSeconds(reloadtime);
		if(tempAmmo < 0)
		{
			ammoCase += ammo;
			ammo = 0;
		}
		else
		{
			ammo -= tempNeededAmmo;
			ammoCase = maxAmmoCase;
		}
		m_reloading = false;
	}
}