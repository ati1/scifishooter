using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour
{

	public int playerAmount = 10;
	public GUISkin customSkin = null;
	private string typeName = "ScifiScene";
	private string gameName = "";

	public Transform[] spawnSpots = null;
	private int m_id = 0;
	public string playerName = "";

	public float searchTime = 0;
	public float maxSearchTime = 0;

	private bool isRefreshingHostList = false;
	private List<HostData> hostList = new List<HostData>();
	
	public int classID;
	public bool isOnClassSelection = false;
	
	public GameObject playerPrefab;
	public Camera playerCamera;
	public Camera menuCamera;

	public Player player;

	private bool selection = true;
	[HideInInspector]
	public bool cameraSetUp = false;


	public GameObject UI;
	public Transform buttonHolder;
	public Button hostDataButton;

	public InputField playerNameField, serverNameField;
	
	void Awake() 
	{
		typeName += "/" + Application.loadedLevel;
		InitUI ();
	}

	public void InitUI()
	{
		if(!cameraSetUp)
		{
			UI.SetActive(true);
			cameraSetUp = true;
			Cursor.visible = true;
			menuCamera.gameObject.SetActive(true);
		}
	}

	void OnGUI()
	{
		GUI.skin = customSkin;
//		if (!Network.isClient && !Network.isServer)
//		{
//			if(!cameraSetUp)
//			{
//				cameraSetUp = true;
//				Cursor.visible = true;
//				menuCamera.gameObject.SetActive(true);
//			}
//
//			if(selection)
//			{
//				GUI.Label(new Rect(20,45,100,25), "Your Name: ");
//				playerName = GUI.TextArea(new Rect(100,45,250,25),playerName);
//				GUI.Label(new Rect(100,75,150,25), "Character Color");
//				if (GUI.Button(new Rect(100, 100, 150, 25), "Red"))
//					m_id = 0;
//				if (GUI.Button(new Rect(100, 145, 150, 25), "Blue"))
//					m_id = 1;
//				if (GUI.Button(new Rect(100, 190, 150, 25), "Green"))
//					m_id = 2;
//				if (GUI.Button(new Rect(100, 235, 150, 25), "Yellow"))
//					m_id = 3;
//				if (GUI.Button(new Rect(100, 295, 150, 25), "Next"))
//					selection = false;
//			}
//			if(!selection)
//			{
//				GUI.Label(new Rect(20,145,100,25), "Server Name: ");
//				gameName = GUI.TextArea(new Rect(100,145,250,25),gameName);
//				if (GUI.Button(new Rect(100, 100, 150, 25), "Make Server") && gameName != "" && playerName != "")
//					StartServer();
//				if (GUI.Button(new Rect(450, 100, 200, 25), "Refresh Hosts"))
//					RefreshHostList();
//				if (GUI.Button(new Rect(100, 250, 150, 25), "Back"))
//					selection = true;
//			}
//
//			if (hostList != null)
//			{
//				for (int i = 0; i < hostList.Count; i++)
//				{
//					if (GUI.Button(new Rect(400, 140 + (110 * i), 300, 35), hostList[i].gameName) && playerName != "")
//						JoinServer(hostList[i]);
//				}
//			}
//			
//			if(isOnClassSelection && classID == 0)
//			{
//				if (GUI.Button(new Rect(500, 100, 250, 25), "Spawn"))
//					SpawnPlayer();
//			}
//			if(GUI.Button (new Rect(100, 450, 150, 25),"Menu"))
//			{
//				Application.LoadLevel(0);
//			}
//			if(GUI.Button (new Rect(100, 550, 150, 25),"Quit"))
//			{
//				Application.Quit();
//			}
//		}
//		else
//		{
			if(Network.isServer && Input.GetKey(KeyCode.Escape))
			{
				Cursor.visible = true;
				if(GUI.Button (new Rect(500,500,100,35),"Close server"))
				{
					DisconnectServer();
				}
			}
			else
				Cursor.visible = false;
//		}
	}

	public void ChangeMaterial(int id)
	{
		switch(id)
		{
		case 0:
			m_id = 0;
			break;
		case 1:
			m_id = 1;
			break;
		case 2:
			m_id = 2;
			break;
		case 3:
			m_id = 3;
			break;
		}
	}
	
	public void StartServer()
	{
		if(gameName != "" && playerName != "")
		{
			Network.InitializeServer(playerAmount, 25000, Network.HavePublicAddress());
			MasterServer.RegisterHost (typeName, gameName);
			UI.SetActive(false);
		}
	}
	
	void OnServerInitialized()
	{
		Debug.Log ("Server Initialized");
		SpawnPlayer();
	}
	
	void Update()
	{
		gameName = serverNameField.text;
		playerName = playerNameField.text;

		if (isRefreshingHostList) 
		{
			if (searchTime <= 0) 
			{
				searchTime = 0;
			}
			if (searchTime == 0) 
			{
				isRefreshingHostList = false;
				searchTime = maxSearchTime;
			}
			if (MasterServer.PollHostList().Length > 0)
			{
				isRefreshingHostList = false;
				hostList.Clear();
				hostList.AddRange (MasterServer.PollHostList());
			}
			searchTime -= Time.deltaTime;
		}
		else
		{
			HostData[] tempHostData = MasterServer.PollHostList();
			if(tempHostData.Length != hostList.Count)
			{
				hostList.Clear();
				if(tempHostData.Length > 0)
				{
					hostList.AddRange(tempHostData);
				}
			}

			foreach(HostData data in hostList)
			{
				CreateButton(data.gameName, hostList.IndexOf(data));
			}
		}
	}

	public void CreateButton(string buttonText, int index)
	{
		GameObject button = (GameObject)GameObject.Instantiate (hostDataButton.gameObject);
		button.GetComponent<RectTransform>().SetParent(buttonHolder.transform);
		button.GetComponentInChildren<Text> ().text = buttonText;
		
		button.GetComponent<Button> ().onClick.AddListener(() => { 
			
			JoinServer(hostList[index]);
			
		});
	}
	
	public void RefreshHostList()
	{
		if (!isRefreshingHostList)
		{
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}
	
	
	public void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
		UI.SetActive(false);
	}
	
	void OnConnectedToServer()
	{
		SpawnPlayer();
	}

	public void SpawnPlayer()
	{
		if(spawnSpots == null)
		{
			Debug.LogError("Spawnspot array is empty");
			return;
		}

		GameObject myPlayerGO = Network.Instantiate(playerPrefab,GetRandomSpawnPoint().position,Quaternion.identity,0) as GameObject;
		//myPlayerGO.GetComponent<Player>().playerCamera = playerCamera.gameObject;
		myPlayerGO.GetComponent<NetworkView> ().RPC("SetPlayerName",RPCMode.AllBuffered,playerName);
		//myPlayerGO.GetComponentInChildren<PlayerNameText>().networkView.RPC("SetPlayerName3dText",RPCMode.AllBuffered);
		
		myPlayerGO.GetComponent<Inventory>().enabled = true;
		myPlayerGO.GetComponent<Player>().enabled = true;
		myPlayerGO.GetComponent<AnimationScript>().enabled = true;

		//myCamera.SetActive(true);

		myPlayerGO.GetComponent<NetworkView>().RPC ("SetMaterial", RPCMode.AllBuffered, m_id);
		menuCamera.gameObject.SetActive (false);

		player = myPlayerGO.GetComponent<Player>();
		
	}
	void DisconnectServer()
	{
		if (!Network.isServer) 
		{
			return;
		}
		Player[] tempPlayers = FindObjectsOfType<Player>();
		foreach(Player temp in tempPlayers)
		{
			temp.DisconnectPlayer();
		}
		Network.Disconnect();
		MasterServer.UnregisterHost();
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		if (Network.isServer) 
		{
			Network.RemoveRPCs (player);
			Network.DestroyPlayerObjects (player);
		}
	}
	                   
	public Transform GetRandomSpawnPoint()
	{
		Transform spawnSpot = spawnSpots[Random.Range(0,spawnSpots.Length)];
		return spawnSpot;
	}
}
