﻿using UnityEngine;
using System.Collections;

public class AmmoPickupSpawner : MonoBehaviour {

	public GameObject ammoPickup = null;
	public Transform[] spawnPoints = null;
	public bool spawnAmmo = false;
	public float timer = 0;
	public float maxTimer = 0;

	void Start()
	{
		timer = maxTimer;
	}
	void Update()
	{
		if(timer <= 0)
			timer = 0;

		if(timer == 0)
			spawnAmmo = true;

		if(spawnAmmo)
		{
			SpawnAmmo();
		}
		timer--;
	}
	void SpawnAmmo()
	{
		spawnAmmo = false;
		int randomSpawnPoint = Random.Range(0,spawnPoints.Length);
		GameObject.Instantiate(ammoPickup,spawnPoints[randomSpawnPoint].position,Quaternion.identity);
	}
}