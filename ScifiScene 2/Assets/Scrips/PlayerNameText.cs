using UnityEngine;
using System.Collections;

public class PlayerNameText : MonoBehaviour {

	public GameObject player = null;
	public TextMesh thisObject = null;
	private Transform m_thisObjTransform;

	void Start()
	{
		thisObject.text = player.GetComponent<Player> ().player;
		m_thisObjTransform = transform;

	}
	void LateUpdate () 
	{
		m_thisObjTransform.LookAt(transform.root.GetChild(0).position);
		//networkView.RPC ("LookAtPLayerName", RPCMode.All);
	}
//	[RPC] void LookAtPLayerName()
//	{
//		if(networkView.isMine)
//			thisObject.transform.LookAt(transform.root.GetChild(0).position);
//		else
//			thisObject.transform.LookAt(transform.root.GetComponentInChildren<Camera>().transform.position);
//	}
}