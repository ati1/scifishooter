﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Menu : MonoBehaviour {
	
	[System.Serializable]
	public class MainMenuButton
	{
		public Rect buttonPosition = new Rect(0,0,0,0);
		public Texture buttonTexture = null;
		public string buttonText = "";
		public int buttonID = 0;
		public GUIStyle customStyle = null;
	}
	[System.Serializable]
	public class LevelSelectButton : MainMenuButton{}
	[System.Serializable]
	public class OptionsSelectButton : MainMenuButton{}
	
	public GUISkin customGuiSKin;
	public Rect textAreaRect = new Rect(0,0,0,0);
	
	public MainMenuButton[] mainMenuButtons = null;
	public LevelSelectButton[] levelSelectButtons = null;
	public OptionsSelectButton[] optionsSelectButtons = null;
	
	public GameObject logo = null;
	public Vector3 logoPos = new Vector3(0,0,0);
	
	public bool showMainMenu = false;
	public bool showLevelSelect = false;
	public bool showOptions = false;
	public bool showCredits = false;
	
	void OnGUI()
	{
		if(customGuiSKin != null)
			GUI.skin = customGuiSKin;
		
		if(showMainMenu)
		{
			if(GameObject.Find(logo.name + "(Clone)") == false)
				GameObject.Instantiate(logo, logoPos, Quaternion.identity);
			
			GUI.BeginGroup(textAreaRect);
			foreach(MainMenuButton button in mainMenuButtons)
			{
				if(button.buttonTexture == null)
				{
					if(GUI.Button(button.buttonPosition,button.buttonText,button.customStyle))
						DoButtons(button.buttonID);
				}
				else 
				{
					if(GUI.Button(button.buttonPosition,button.buttonTexture,button.customStyle))
						DoButtons(button.buttonID);
				}
			}
			GUI.EndGroup();
		}
		if(showLevelSelect)
		{
			if(GameObject.Find(logo.name + "(Clone)") == false)
				GameObject.Instantiate(logo, logoPos, Quaternion.identity);
			
			GUI.BeginGroup(textAreaRect);
			foreach(LevelSelectButton button in levelSelectButtons)
			{
				if(button.buttonTexture == null)
				{
					if(GUI.Button(button.buttonPosition,button.buttonText,button.customStyle))
						DoButtons(button.buttonID);
				}
				else 
				{
					if(GUI.Button(button.buttonPosition,button.buttonTexture,button.customStyle))
						DoButtons(button.buttonID);
				}
			}
			GUI.EndGroup();
		}
		if(showOptions)
		{
			if(GameObject.Find(logo.name + "(Clone)") == false)
				GameObject.Instantiate(logo, logoPos, Quaternion.identity);
			
			GUI.BeginGroup(textAreaRect);
			foreach(OptionsSelectButton button in optionsSelectButtons)
			{
				if(button.buttonTexture == null)
				{
					if(GUI.Button(button.buttonPosition,button.buttonText,button.customStyle))
						DoButtons(button.buttonID);
				}
				else 
				{
					if(GUI.Button(button.buttonPosition,button.buttonTexture,button.customStyle))
						DoButtons(button.buttonID);
				}
			}
			GUI.EndGroup();
		}
	}
	void DoButtons(int buttonID)
	{
		//do main menu buttons
		if(showMainMenu)
		{
			if(buttonID == 0)
				ShowMenu(1);
			if(buttonID == 1)
				ShowMenu(2);
			if(buttonID == 2)
				ShowMenu(3);
			if(buttonID == 3)
				ShowMenu(4);
		}
		//do level select buttons
		else if(showLevelSelect)
		{
			if(buttonID == 0)
				ShowMenu(0);
		}
		//do options select buttons
		else if(showOptions)
		{
			if(buttonID == 0)
				ShowMenu(0);
		}
	}
	public void ShowMenu(int menuID)
	{
		switch (menuID)
		{
			case 0: showMainMenu = true; showLevelSelect = false; showOptions = false; showCredits = false; break;
			case 1: showMainMenu = false; showLevelSelect = true; showOptions = false; showCredits = false; break;
			case 2: showMainMenu = false; showLevelSelect = false; showOptions = true; showCredits = false; break;
			case 3: showMainMenu = false; showLevelSelect = false; showOptions = false; showCredits = true; break;
			case 4: Application.Quit(); break;
		}
	}
}