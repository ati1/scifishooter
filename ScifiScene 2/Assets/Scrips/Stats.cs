﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stats : MonoBehaviour {

	public GUISkin customSkin = null;
	[System.Serializable]
	public class Health
	{
		public float health = 0;
		public float maxHealth = 100;
	}
	
	public Health health;
	public int deaths = 0;
	public bool displayHealth = false;
	public Rect HealthPosition = new Rect(0,0,0,0);
	public List<Inventory.AmmoAmount> firstAmmos = null;
	public Player lastHitted = null;
	public bool dead = false;
	
	public float maxRespawnTimer = 0;
	public float respawnTimer = 0;

	[RPC]
	public void AdjustHealth(float healthChange)
	{
		float tempHealth = health.health + healthChange;
		Debug.Log("Upcoming health change: " + tempHealth.ToString());
		if(tempHealth > 0)
			health.health += healthChange;
		else if(tempHealth <= 0 && !dead)
		{
			health.health = 0;
			GetComponent<NetworkView>().RPC("Die",RPCMode.All);
		}
	}
	void Start()
	{
		if(gameObject.GetComponent<Inventory>())
		{
			foreach(Inventory.AmmoAmount ammo in gameObject.GetComponent<Inventory>().ammoAmountList)
			{
				firstAmmos.Add(ammo.AmmoCopy());
			}
		}
	}

	void OnGUI()
	{
		GUI.skin = customSkin;
		if(GetComponent<NetworkView>().isMine)
		{
			if(displayHealth)
			{
				GUI.Box(HealthPosition, health.health.ToString());
			}
			if(Input.GetKey(KeyCode.Escape))
			{
				if(GUI.Button(new Rect(500,400,75,75),"Suicide"))
				{
					GetComponent<NetworkView>().RPC("Die",RPCMode.All);
				}
			}
			   
			if(dead)
				GUI.Label(new Rect(400,350,200,45),"YOU HAVE DIED.. Respawn in: " + (int)respawnTimer);
		}
	}
	
	[RPC] public void Die()
	{
		if(gameObject.GetComponent<Player>())
		{
			if(GetComponent<NetworkView>().isMine)
			{
				dead = true;
			}
			else
			{
				FindObjectOfType<ScoreBoard> ().RefresScoreboard ();
			}
		}
		else
		{
			if(GetComponent<NetworkView>())
				Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
			Network.Destroy(gameObject);
		}
	}
	[RPC] public void SetLastHitted(string playerName)
	{
		Player[] players = FindObjectsOfType<Player> ();
		foreach (Player player in players) 
		{
			if(player.player == playerName)
			{
				lastHitted = player;
			}
		}
	}
	[RPC] public void GivePointsToPlayer()
	{
		if (lastHitted != null)
			lastHitted.score += 1;

		deaths += 1;
		GetComponent<Player> ().deaths = deaths;

		FindObjectOfType<ScoreBoard> ().RefresScoreboard ();
	}

	void Update()
	{
		if (GetComponent<NetworkView>().isMine) 
		{
			if (dead) 
			{
				//dead animation
				GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
				GetComponent<Player> ().enableMovements = false;
				GetComponentInChildren<Gun>().isEnabled = false;
				
				if(respawnTimer <= 0)
				{
					Respawn();
					dead = false;
					respawnTimer = maxRespawnTimer;
				}
				respawnTimer -= Time.deltaTime;
			}
		}
	}
	public void Respawn()
	{	
		GetComponent<NetworkView>().RPC ("GivePointsToPlayer", RPCMode.AllBuffered);
		GetComponentInChildren<Gun>().isEnabled = true;
		GetComponent<Player> ().enableMovements = true;
		Transform spawnSpot = FindObjectOfType<NetworkManager>().GetRandomSpawnPoint();
		transform.position = spawnSpot.position;
		GetComponent<NetworkView>().RPC ("ResetHPToMax", RPCMode.All);
		GetComponent<Inventory>().ResetAmmoAmount(firstAmmos);

	}
	[RPC] public void ResetHPToMax()
	{
		health.health = health.maxHealth;
	}
}