﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {
	
	public float length = 0;
	public GameObject crosshair;

	private float m_maxRange = 100f;
	
	private Transform aimTarget;
	
	private bool raycastHit = false;


	void Start()
	{
		float range = transform.root.GetComponentInChildren<Gun> ().range;
		if (range <= m_maxRange)
			length = range;
		else
			length = m_maxRange;

		crosshair = GameObject.Instantiate(crosshair,transform.position,Quaternion.identity) as GameObject;
		crosshair.transform.parent = transform;
		if(transform.root.GetComponentInChildren<cameraOrbit>())
		{
			aimTarget = transform.root.GetComponentInChildren<cameraOrbit>().thisCamera.transform;
		}
	}
	void Update()
	{
		RaycastHit hit;
		raycastHit = Physics.Raycast(transform.position,transform.forward,out hit,length);

		if(raycastHit)
			crosshair.transform.position = hit.point;
		else
			crosshair.transform.position = transform.position + transform.forward * length;

		if(aimTarget!= null)
			crosshair.transform.LookAt(aimTarget);
	}
}