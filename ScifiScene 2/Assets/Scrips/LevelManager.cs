﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {
	
	public List<Player> GetAllPlayers()
	{
		Player[] temp = FindObjectsOfType<Player> ();
		List<Player> players = new List<Player> ();
		players.AddRange (temp);
		foreach (Player player in players) 
		{
			player.playerId = players.IndexOf(player);
		}
		return players;
	}
}