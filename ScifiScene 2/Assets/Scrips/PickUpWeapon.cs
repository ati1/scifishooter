﻿using UnityEngine;
using System.Collections;

public class PickUpWeapon : MonoBehaviour {
	
	public Weapon weaponPrefab = null;
	public Rect popUpRect = new Rect(0,0,0,0);
	
	private bool m_showPopUp = false;
	private bool m_takeWeapon = false;
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
			if(col.gameObject.GetComponent<Inventory>().IsInventoryFull())
				m_showPopUp = true;
		
	}
	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
			if(m_takeWeapon && col.gameObject.GetComponent<Inventory>().IsInventoryFull())
		{
			col.gameObject.GetComponent<Inventory>().AddWeaponToInventory(weaponPrefab);
			Destroy(gameObject);
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			m_showPopUp = false;
			m_takeWeapon = false;
		}
	}	
	void OnGUI()
	{
		if(m_showPopUp)
		{
			if(GUI.Button(popUpRect,"Pick up: " + weaponPrefab.name))
			{
				m_showPopUp = false;
				m_takeWeapon = true;
			}
		}
	}
}