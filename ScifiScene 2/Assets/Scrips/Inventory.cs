﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	public Transform weaponPos = null;
	public Rect guiAreaRect = new Rect(0,0,0,0);
	public GameObject pickUpWeapon = null;
	public List<Weapon> myWeaponList = null;
	public int maxWeaponAmount = 0;
	
	private GameObject m_tempPickUpWeapon = null;
	private List<Weapon> m_removeWeaponList = new List<Weapon>();

	public int weaponId = 0;
	public Weapon currentWeapon = null;
	private GameObject currentWeaponGO;
	

	//KeyCode numbers;
	List<KeyCode> numberss = new List<KeyCode>{ KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3,
		KeyCode.Alpha4, KeyCode.Alpha5};

	[System.Serializable]
	public class AmmoAmount
	{
		public string ammoName = "";
		public int ammoAmount = 0;
		public int maxAmmoAmount = 0;
		public GameObject ammoGO;
		public int ammoID = 0;

		public AmmoAmount AmmoCopy()
		{
			AmmoAmount newAmmoAmount = new AmmoAmount();
			newAmmoAmount.ammoName = ammoName;
			newAmmoAmount.ammoAmount = ammoAmount;
			newAmmoAmount.ammoID = ammoID;
			return newAmmoAmount;
		}
	}
	public List<AmmoAmount> ammoAmountList = null;

	void Start()
	{
		if(GetComponent<NetworkView>().isMine)
		{
			currentWeapon = myWeaponList[0];
			ChangeWeapon(currentWeapon);
		}
	}
	public void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		int syncWepId = weaponId;
		if (stream.isWriting) 
		{
			stream.Serialize(ref syncWepId);
		} 
		else
		{
			stream.Serialize(ref syncWepId);
			weaponId = syncWepId;

			switch(weaponId)
			{
			case 0:
				currentWeapon = myWeaponList[0];
				break;
			case 1:
				currentWeapon = myWeaponList[1];
				break;
			case 2:
				currentWeapon = myWeaponList[2];
				break;
			case 3:
				currentWeapon = myWeaponList[3];
				break;
			case 4:
				currentWeapon = myWeaponList[4];
				break;

			}
		}
	}

	void Update()
	{
		ChangeWeaponWithNumbers ();
	}
	

//	void OnGUI()
//	{
//		if(networkView.isMine)
//		{
//			GUILayout.BeginArea(guiAreaRect);
//			GUILayout.BeginVertical();
//
//			foreach(Weapon weapon in myWeaponList)
//			{
//				int indexOf = myWeaponList.IndexOf(weapon);
//				if(weapon != null)
//				{
//					GUILayout.BeginHorizontal();
//					if(GUILayout.Button(weapon.weaponLogo,GUILayout.Width(50),GUILayout.Height(50)))
//					{
//						currentWeapon = weapon;
//						//networkView.RPC("ChangeWeapon", RPCMode.AllBuffered,weapon);
//						ChangeWeapon(weapon);
//					}
//					GUILayout.BeginVertical();
//					if(GUILayout.Button("X",GUILayout.Width(25)))
//					{
//						//networkView.RPC("RemoveWeaponFromInventory", RPCMode.AllBuffered,weapon);
//						RemoveWeaponFromInventory(weapon);
//					}
//
//
//
//					GUILayout.Label((indexOf + 1).ToString(),GUILayout.Width(25));
//					GUILayout.EndVertical();
//					GUILayout.EndHorizontal();
//				}
//			}
//
//			if(m_removeWeaponList.Count > 0)
//			{
//				foreach(Weapon weapon in m_removeWeaponList )
//				{                                
//					int indexOf = myWeaponList.IndexOf(weapon);
//					myWeaponList.RemoveAt(indexOf); 
//				}
//				m_removeWeaponList.Clear();
//			}
//
//			GUILayout.EndVertical();
//			GUILayout.EndArea();
//		}
//	}
	
	public void AddWeaponToInventory(Weapon weapon)
	{
		myWeaponList.Add(weapon);
	}
	[RPC]public void ChangeWeaponWithNumbers()
	{
		for(int i =0;i<numberss.Count;i++)
		{
			if(GetComponent<NetworkView>().isMine)
			{
				if(Input.GetKeyDown(numberss[i]))
				{
					currentWeapon = myWeaponList[i];
					Gun temp = currentWeapon.GetComponent<Gun>();
					ChangeWeapon(currentWeapon);
					weaponId = temp.weaponId;
				}
			}
			else
			{
				SyncWeapon();
			}
		}
	}
	public void SyncWeapon()
	{
		if (currentWeaponGO != null) 
		{
			if (currentWeapon.name + "(Clone)" == currentWeaponGO.name)
				return;

			Destroy (currentWeaponGO);
		}
		if (currentWeapon != null) 
		{
			currentWeaponGO = GameObject.Instantiate (currentWeapon.gameObject, weaponPos.position, Quaternion.identity) as GameObject;
			//error
//			currentWeaponGO.GetComponentInChildren<Crosshair> ().gameObject.SetActive (false);
			currentWeaponGO.transform.parent = weaponPos;
			currentWeaponGO.transform.position = weaponPos.position;
			currentWeaponGO.GetComponent<Gun>().ammoList = ammoAmountList;
			//currentWeaponGO.GetComponent<Gun>().muzzleFlare = GetComponentInChildren<ParticleSystem>();
			currentWeaponGO.transform.localRotation = Quaternion.Euler (75f, 75f, 75f);
			currentWeaponGO.GetComponent<Gun> ().isEnabled = false;
			currentWeaponGO.GetComponent<Gun> ().enabled = false;
		}

	}
	//[RPC]
	public void ChangeWeapon(Weapon newWeapon)
	{
		foreach(AmmoAmount ammoAmount in ammoAmountList)
		{
			if(ammoAmount != null && GetComponentInChildren<Gun>())
			{
				if(GetComponentInChildren<Gun>().ammoName == ammoAmount.ammoName)
				{
					int tempAmmo = GetComponentInChildren<Gun>().ReturnAmmo(ammoAmount.ammoName);
					if(tempAmmo > ammoAmount.maxAmmoAmount)
						ammoAmount.ammoAmount = ammoAmount.maxAmmoAmount;
					else
						ammoAmount.ammoAmount = tempAmmo;
				}
			}
		}
//
//		if (currentWeaponGO != null && currentWeaponGO.GetComponent<Gun>())
//			Network.RemoveRPCs (currentWeaponGO.networkView.viewID);

		Destroy (currentWeaponGO);
		currentWeaponGO = GameObject.Instantiate(newWeapon.gameObject,Vector3.zero,Quaternion.identity) as GameObject;
		currentWeaponGO.transform.parent = weaponPos;
		//currentWeaponGO.transform.localPosition = Vector3.zero + new Vector3(1f,0,0);
		currentWeaponGO.transform.position = weaponPos.position;
		currentWeaponGO.transform.localRotation = Quaternion.Euler(75f,75f,75f);
	}
	//[RPC]
	public void RemoveWeaponFromInventory(Weapon weapon)
	{
		m_tempPickUpWeapon = pickUpWeapon;
		m_tempPickUpWeapon = GameObject.Instantiate(m_tempPickUpWeapon,transform.position,Quaternion.identity) as GameObject;
		m_tempPickUpWeapon.GetComponent<PickUpWeapon>().weaponPrefab = weapon;
		m_removeWeaponList.Add(weapon);
	}
	public bool IsInventoryFull()
	{
		bool isInventoryFull = false;
		if(myWeaponList.Count >= maxWeaponAmount)
			isInventoryFull = true;
		return isInventoryFull;
	}
	public int SendAmmo(string newAmmoName)
	{
		int returnValue = 0;
		foreach(AmmoAmount ammoAmount in ammoAmountList)
		{
			if(ammoAmount.ammoName == newAmmoName)
				returnValue = ammoAmount.ammoAmount;
		}
		return returnValue;
	}
	public GameObject SendAmmoGO(string newAmmoName)
	{
		GameObject returnValue = null;
		foreach(AmmoAmount ammoAmount in ammoAmountList)
		{
			if(ammoAmount.ammoName == newAmmoName)
				returnValue = ammoAmount.ammoGO;
		}
		return returnValue;
	}
	public Weapon GetCurrentWeapon()
	{
		return currentWeapon;
	}
	public void ResetAmmoAmount(List<Inventory.AmmoAmount> newAmmoList)
	{
		foreach(AmmoAmount ammo in ammoAmountList)
		{
			foreach(AmmoAmount newAmmo in newAmmoList)
			{
				if(ammo.ammoName == newAmmo.ammoName)
				{
					ammo.ammoAmount = newAmmo.ammoAmount;
				}
			}
		}
//		weaponPos = newInventory.weaponPos;
//		guiAreaRect = newInventory.guiAreaRect;
//		pickUpWeapon = newInventory.pickUpWeapon;
//		myWeaponList = newInventory.myWeaponList;
//		maxWeaponAmount = newInventory.maxWeaponAmount;
//		m_tempPickUpWeapon = newInventory.m_tempPickUpWeapon;
//		m_removeWeaponList = newInventory.m_removeWeaponList;
//		currentWeapon = newInventory.currentWeapon;
//		currentWeaponGO = newInventory.currentWeaponGO;
//
//		foreach(Inventory.AmmoAmount ammo in ammoAmountList)
//		{
//			Debug.Log("Reseted inventory: " + " Ammo name: " + ammo.ammoName + " Amount: " + ammo.ammoAmount);
//		}
	}
	public List<AmmoAmount> GetAmmoList()
	{
		return ammoAmountList;
	}
	public bool IsAmmoFull(string ammoName)
	{
		bool isAmmoFull = false;
		foreach(AmmoAmount ammo in ammoAmountList)
		{
			if(ammo.ammoName == ammoName)
			{
				if(ammo.ammoAmount >= ammo.maxAmmoAmount)
					isAmmoFull = true;
			}
				
		}

		return isAmmoFull;
	}
}