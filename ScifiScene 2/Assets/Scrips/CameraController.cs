﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	
	private Transform m_transform;
	public Transform playerTransform;
	
	private float m_x;
	private float m_y;
	
	public float xSpeed;
	public float ySpeed;
	
	public float minY;
	public float maxY;
	
	private Vector3 m_angles;
	
	public float distance;
	
	
	private void Awake()
	{
		// cache transform and angles
		m_transform = transform;
		
		m_angles = m_transform.eulerAngles;
		m_x = m_angles.y;
		m_y = m_angles.x;
	}
	// we use fixed update so physics calculations happen at the right time.
	private void FixedUpdate () 
	{
		//we get x/y axises and time them by speed.
		m_x += Input.GetAxis("Mouse X") * xSpeed;
		m_y -= Input.GetAxis("Mouse Y") * ySpeed;
		// get rotation based on x/y
		Quaternion rotation = Quaternion.Euler(m_y, m_x, 0);
		// we clamp the values so they don't exceed a certain values.
		m_y = ClampAngle(m_y, minY, maxY);
		// set the rotation of camera
		m_transform.rotation = rotation;
		// calculate the position of camera based on distance and rotation
		m_transform.position = rotation * new Vector3 (0, 0, -distance) + playerTransform.position;
		// set player x rotation based on camera.
		if(playerTransform != null)
			playerTransform.localEulerAngles = new Vector3(playerTransform.localEulerAngles.x, m_transform.localEulerAngles.y, m_transform.localEulerAngles.z); 
		
	}
	/// <summary>
	/// Clamps the angle according to min/max values.
	/// </summary>
	/// <returns>The angle.</returns>
	/// <param name="angle">Angle.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static float ClampAngle (float angle, float min, float max)	
	{
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		
		return Mathf.Clamp (angle, min, max);
	}
}