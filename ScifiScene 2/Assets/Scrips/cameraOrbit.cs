﻿using UnityEngine;
using System.Collections;

public class cameraOrbit : MonoBehaviour {
	
	public Camera thisCamera = null;
	public Transform lookAt;
	public float distance = 5.0f;
	float x = 0.0f;
	float y = 0.0f;
	
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	
	public float distanceMin = 0.5f;
	public float distanceMax = 15f;
	private bool click = false;
	private float curDist = 0;

	public Vector3 angles;

	public Transform playerTransform;


	private void Start()
	{
		playerTransform = lookAt.root;
		angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;

	}
//	void OnNetworkInstantiate(NetworkMessageInfo info)
//	{
//		if(networkView.isMine)
//			thisCamera.enabled = true;
//		else
//			thisCamera.enabled = false;
//	}
	
	private void LateUpdate () 
	{
		x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
		y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
		
		Quaternion rotation = Quaternion.Euler(y, x, 0);
		distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel")*5, distanceMin, distanceMax);
		
		Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
		Vector3 position = rotation * negDistance + lookAt.position;

		transform.rotation = rotation;
		transform.position = position;
		
		float cameraX = transform.rotation.x;
		playerTransform.eulerAngles = new Vector3 (cameraX, transform.eulerAngles.y, transform.eulerAngles.z);

		if(playerTransform.GetComponentInChildren<Gun>())
		{
			Transform weaponTransform = playerTransform.GetComponentInChildren<Gun>().transform;
			//float xRot = transform.eulerAngles.x;
			//weaponTransform.eulerAngles = new Vector3(ClampAngle(xRot,-4,4) * 10 ,transform.eulerAngles.y,transform.eulerAngles.z);
			weaponTransform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z);
			//Debug.Log(xRot);
		}		
		if (Input.GetMouseButtonDown (2)) 
		{
			if (!click) 
			{
				click = true;
				curDist = distance;
				distance = distance - distance - 1;
			} 
			else
			{
				distance = curDist;
				click = false;
			}
		}

		RaycastHit hit;
		if(Physics.Raycast(lookAt.position,(transform.position - lookAt.position).normalized,out hit,(distance <= 0 ? -distance : distance)))	
		{
			transform.position = hit.point - (transform.position - hit.point).normalized * 1.2f;
		}
	}
	public static float ClampAngle (float angle, float min, float max)	
	{
		if (angle < -180f)
			angle += 360F;
		
		if (angle > 180f)
			angle -= 360F;
		
		return Mathf.Clamp (angle, min, max);
	}
}
