﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ScoreBoard : MonoBehaviour {
	
	[System.Serializable]
	public class score
	{
		public string name;
		public int kills;
		public int deaths;
	}

	public List<score> scores;

	public void RefresScoreboard()
	{
		scores.Clear();
		Player[] players = FindObjectsOfType<Player> ();
		for (int i = 0; i<players.Length;i++) 
		{
			score score = new score();
			score.name = players[i].player;
			score.kills = players[i].score;
			score.deaths = players[i].deaths;
			scores.Add(score);
		}
		scores = scores.OrderByDescending(w => w.kills).ToList();
	}
}