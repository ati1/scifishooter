﻿using UnityEngine;
using System.Collections;

public class NewCrosshair : MonoBehaviour {

	public Texture crosshairtext;

	void OnGUI()
	{
		if(GetComponent<NetworkView>().isMine)
			GUI.DrawTexture (new Rect (Screen.width / 2 - 10, Screen.height / 2 + 10, 15, 15), crosshairtext);
	}
}