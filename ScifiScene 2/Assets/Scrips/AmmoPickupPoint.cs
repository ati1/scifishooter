﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoPickupPoint : MonoBehaviour {

	private bool m_showPopUp = false;

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			if(col.gameObject.GetComponent<Player>().GetComponent<NetworkView>().isMine)
				m_showPopUp = true;
		}
		
	}
	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			if(col.gameObject.GetComponent<Player>().GetComponent<NetworkView>().isMine)
			{
				if(Input.GetKeyDown(KeyCode.E))
				{
					Inventory inventory = col.gameObject.GetComponent<Inventory>();
					inventory.ResetAmmoAmount(col.gameObject.GetComponent<Stats>().firstAmmos);
					Gun currentWep = col.gameObject.GetComponentInChildren<Gun>();
					foreach (Inventory.AmmoAmount ammoAmount in inventory.ammoAmountList)
					{
						if(ammoAmount.ammoName == currentWep.ammoName)
						{
							currentWep.ammo = ammoAmount.maxAmmoAmount;
						}
					}
				}
			}
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.GetComponent<Player>())
		{
			if(col.gameObject.GetComponent<Player>().GetComponent<NetworkView>().isMine)
				m_showPopUp = false;
		}
	}
	void OnGUI()
	{
		if (m_showPopUp) 
		{
			GUI.Box (new Rect (400, 300, 200, 35), "Press E to full ammos");
		}
	}
}