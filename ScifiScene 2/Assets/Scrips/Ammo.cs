﻿using UnityEngine;
using System.Collections;

public class Ammo : MonoBehaviour {
	
	//public Transform weapon = null;
	public float ammoSpeed = 0;
	public float ammoDmg = 0;
	public Player shooter = null;
	
	public float ammoLifeTime = 0;
	public float maxAmmoLifeTime = 0;
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.GetComponent<Stats>())
		{
			if(col.gameObject.GetComponent<Player>())
			{
				col.gameObject.GetComponent<Stats>().lastHitted = shooter;
			}
			col.gameObject.GetComponent<NetworkView>().RPC ("AdjustHealth", RPCMode.AllBuffered, -ammoDmg);
			//particles
			Destroy(gameObject);
		}
		else
		{
			//particles
			Destroy(gameObject);
		}
	}
//	void Start()
//	{
//		rigidbody.AddForce(weapon.forward * ammoSpeed);
//	}
	void Update()
	{
		ammoLifeTime += Time.deltaTime;
		
		if(ammoLifeTime > maxAmmoLifeTime)
		{
			ammoLifeTime = 0;
//			Network.RemoveRPCs(networkView.viewID);
//			Network.Destroy(gameObject);
			//DestoroyBullet();
			Destroy(gameObject);
		}
	}
}